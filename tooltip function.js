function(tooltipModel) {
    // Tooltip Element
    var tooltipEl = document.getElementById('chartjs-tooltip');

    // var dropdownElement = "<div class='card' style='width: 18rem;'><ul class='list-group list-group-flush'><li class='list-group-item'>Cras justo odio</li><li class='list-group-item'>Dapibus ac facilisis in</li><li class='list-group-item'>Vestibulum at eros</li></ul></div>";

    // var dropdownElement = "<div class='card text-white bg-info mb-3' style='max-width: 18rem;'><div class='card-header'>Header</div><div class='card-body'><h5 class='card-title'>Info card title</h5><p class='card-text'>Some quick example text to build on the card title and make up the bulk of the card's content.</p></div></div>";

    // var dropdownElement = "<div class='dropdown'><button class='btn btn-secondary dropdown-toggle' type='button' id='dropdownMenu2' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Dropdown</button><div class='dropdown-menu' aria-labelledby='dropdownMenu2'><button class='dropdown-item' type='button'>Action</button><button class='dropdown-item' type='button'>Another action</button><button class='dropdown-item' type='button'>Something else here</button></div></div>";

    var dropdownElement = "<div class='card border-info mb-3' style='max-width: 18rem;'><div class='card-header'>Header</div><div class='card-body text-info'><h5 class='card-title'>Info card title</h5><p class='card-text'>Some quick example text to build on the card title and make up the bulk of the card's content.</p></div></div>";

    var container = "<div></div>";

    // console.log(dropdownElement);

    // Create element on first render
    if (!tooltipEl) {
        tooltipEl = document.createElement('div');
        tooltipEl.id = 'chartjs-tooltip';
        tooltipEl.innerHTML = container;
        document.body.appendChild(tooltipEl);
    }

    // Hide if no tooltip
    if (tooltipModel.opacity === 0) {
        tooltipEl.style.opacity = 0;
        return;
    }

    // Set caret Position
    tooltipEl.classList.remove('above', 'below', 'no-transform');
    if (tooltipModel.yAlign) {
        tooltipEl.classList.add(tooltipModel.yAlign);
    } else {
        tooltipEl.classList.add('no-transform');
    }

    function getBody(bodyItem) {
        return bodyItem.lines;
    }

    // Set Text
    if (tooltipModel.body) {
        // var titleLines = tooltipModel.title || [];
        // var bodyLines = tooltipModel.body.map(getBody);

        // var innerHtml = '<div class=&quot;dropdown-menu&quot;>';

        // titleLines.forEach(function(title) {
        //     innerHtml += '<tr><th>' + title + '</th></tr>';
        // });
        // innerHtml += '</thead><tbody>';

        // bodyLines.forEach(function(body, i) {
        //     var colors = tooltipModel.labelColors[i];
        //     var style = 'background:' + colors.backgroundColor;
        //     style += '; border-color:' + colors.borderColor;
        //     style += '; border-width: 2px';
        //     var span = '<span style="' + style + '"></span>';
        //     innerHtml += '<tr><td>' + span + body + '</td></tr>';
        // });
        // innerHtml += '</tbody>';


        var tableRoot = tooltipEl.querySelector('div');
        // console.log(tableRoot);
        tableRoot.innerHTML = dropdownElement;
    }

    // `this` will be the overall tooltip
    var position = this._chart.canvas.getBoundingClientRect();


    // Display, position, and set styles for font
    tooltipEl.style.opacity = 1;
    tooltipEl.style.position = 'absolute';
    tooltipEl.style.left = position.left + tooltipModel.caretX + 'px';
    tooltipEl.style.top = position.top + tooltipModel.caretY + 'px';
    tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
    tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
    tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
    tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
}

		            